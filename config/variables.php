<?php

/* connect to DB */
try {
    $db = new PDO(
        'mysql:host=localhost;dbname=tp_php;charset=utf8',
        'root',
        'root',
        [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION],
    );
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

/* get messages data frome DB */
$messagesStatement = $db->prepare('SELECT * FROM messages ');
$messagesStatement->execute();
$messages = $messagesStatement->fetchAll();


if(isset($_GET['i']) && is_numeric($_GET['i'])) {
    $i = (int) $_GET['i'];
} else {
    $i = 100;
}

/* roothPath and rootUrl for later usage (include_once etc.) */
$rootPath = $_SERVER['DOCUMENT_ROOT'];
$rootUrl = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';

?>