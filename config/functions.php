<?php
function get_messages(array $messages, int $i): array
{
    $valid_messages = [];
    $counter = 0;

    foreach ($messages as $message) {
        if ($counter == $i) {
            return $valid_messages;
        }

        $valid_messages[] = $message;
        $counter++;
    }

    return $valid_messages;
}

?>

