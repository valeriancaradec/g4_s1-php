<?php
include_once('./../config/variables.php');
include_once('./../config/functions.php');
?>

<header class="header width-100">
  <nav class="header_nav">
    <h1 class="nav_element nav_h1 ">TP php - institut G4</h1>
    <a class="nav_element nav_a" aria-current="page"
      href="<?php echo ($rootUrl) . '/TP_php/page/index.php'; ?>">Home</a>
  </nav>
</header>