<?php
 session_start();

include_once('./../config/variables.php');

$messageId = $_POST['message_id'];

/* sql QUERY to delete message from DB */
$deleteQuery = $db->prepare('DELETE FROM messages WHERE message_id = :message_id ');
$deleteQuery->execute([
    'message_id' => $messageId,
]);

header('Location: ' . $rootUrl . '/tp_php/page/index.php');
?>

