<div>
    <h2>Postez votre message</h2>
    <form class="section-div" action="<?php echo ($rootUrl . '/TP_php/page/submit_msg.php'); ?>" method="POST">
        <div class="post-msg width-100">
            <label class="post-label" for="author">Pseudo</label>
            <div class="post-div">
                <input class="post-input left-" type="text" id="author" name="author" aria-describedby="author-help">
                <div class="info-element left-" id="author-help">Renseignez votre pseudonyme.</div>
            </div>

        </div>
        <div class="post-msg width-100">
            <label class="post-label" for="message">Votre message</label>
            <div class="post-div left-">
                <textarea class=" info-element post-input input-textarea overflow" placeholder="Exprimez vous" id="message"
                    name="message"></textarea>
            </div>
        </div>
        <div class="justify-right">
            <button class="validate-btn btn" type="submit">Envoyer</button>
        </div>
    </form>
    <br />
</div>