<div>
    <h2>Chat communautaire</h2>
    <div class="msg overflow height-35">
        <?php foreach (get_messages($messages, $i) as $message): ?>

            <article class="border-left">
                <!-- show message with info -->
                <h3 class="msg-author">
                    <?php echo ($message['author']); ?> à écrit :
                </h3>
                <div >
                    <!-- WARNING - make sure the pre balise is in one line  -->
                    <pre class="msg-content"><?php echo ($message['message']); ?></pre>
                </div>
                <div class="msg-info">
                    <p class="info-element">
                        Posté le:
                        <?php echo ($message['written_at']); ?>
                    </p>
                        <a class="delete-btn btn info-element" href="./../page/delete_msg.php?message_id=<?php echo ($message['message_id']); ?>">
                            Supprimer le message 
                        </a>

                </div>
            </article>

        <?php endforeach ?>
    </div>

</div>