<?php  
session_start();
include_once('./../config/variables.php');

$getData = $_GET;
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TP php - Supprimer le message ?</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@24,400,0,0" />
    <link rel="stylesheet" href="./../public/style/css/style.css">

    <link rel="icon" type="image/png" href="./../public/style/image/png/favicon-16x16.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="./../public/style/image/png/favicon-32x32.png" sizes="16x16" />

</head>
<body >
    <?php include_once($rootPath.'/tp_php/component/header.php'); ?>
    <section class="section">
        <h2>Supprimer le message ?</h2>
        <form action="<?php echo($rootUrl . 'tp_php/component/post_delete_msg.php'); ?>" method="POST">
            
                <input type="hidden" id="message_id" name="message_id" value="<?php echo($getData['message_id']); ?>">                
            
            <div class="section-div">
                <a class="btn validate-btn" href="<?php echo($rootUrl . 'tp_php/page/index.php')?>">Retour en arrière</a>
                <button class="btn delete-btn" type="submit" >La suppression est définitive</button>

            </div>
            
            </form>
    </section>
    <?php include_once($rootPath.'/tp_php/component/footer.php'); ?>
</body>

</html>