<?php
session_start();

include_once('./../config/variables.php');

$author = $_POST['author'];
$message = $_POST['message'];

/* check if author/msg is empty  */
if (empty($author) || empty($message)) {
    echo ('Le pseudonyme et le message ne peuvent pas être vides.');
    header('refresh:2 ; URL= ' . $rootUrl . '/tp_php/page/index.php');
    return;
}

$insertMessage = $db->prepare('INSERT INTO messages(author, message) VALUES (:author, :message)');
$insertMessage->execute([
    'author' => $author,
    'message' => $message,
]);
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TP php - Message posté</title>

    <link rel="stylesheet" href="./../public/style/css/style.css">

    <link rel="icon" type="image/png" href="./../public/style/image/png/favicon-16x16.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="./../public/style/image/png/favicon-32x32.png" sizes="16x16" />
</head>

<body>
    <?php include_once('./../component/header.php'); ?>
    <section class="section">
        <h2>Message bien reçu !</h2>
        <div class="section-div">
            <p class><b>Pseudonyme</b> :
                <?php echo ($author); ?>
            </p>
            <p><b>Message</b> :
            <pre class="msg-content"><?php echo strip_tags($message); ?></pre>
                
            </p>
        </div>
    </section>
    <?php include_once('./../component/footer.php'); ?>
</body>

</html>

<?php
header("refresh:1 ; URL=./../page/index.php");
?>