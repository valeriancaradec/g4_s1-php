<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>TP php - Acceuil</title>

    <link rel="stylesheet" href="./../public/style/css/style.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@24,400,0,0" />

    <link rel="icon" type="image/png" href="./../public/style/image/png/favicon-16x16.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="./../public/style/image/png/favicon-32x32.png" sizes="16x16" />

</head>

    <?php include_once('./../component/header.php'); ?>
<body>
    <section class="section">
        <?php include_once('./../component/displaychat.php'); ?>
    </section>
    <section class="section">
        <?php include_once('./../component/post_msg.php'); ?>
    </section>
</body>

    <?php include_once('./../component/footer.php'); ?>
</html>